def load(filename):
    output = {}
    with open(filename) as fp:
        for line in fp:
            show, speaker_id, speaker_type = line.strip().split('\t')[:3]
            output[(show, speaker_id)] = speaker_type
    return output

class SpeakerType:
    def __init__(self, mapping_filename):
        self.mapping = load(mapping_filename)

    def resolve(self, show, speaker_id):
        key = (show, speaker_id)
        if key in self.mapping:
            return self.mapping[key]
        return None

if __name__ == '__main__':
    import sys
    speaker_type = SpeakerType(sys.argv[1])
    for line in sys.stdin:
        print speaker_type.resolve(*line.strip().split())

