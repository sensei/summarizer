#-*- coding: utf-8 -*-
# The tsv format was defined for storing Decoda annotations, each word has a few fields

#<filename> <global-wordnum> <wordnum-in-sentence> <word> NULL <postag> NULL NULL <dependency-label> <governor-wordnum> <text-id> <lemma> <morphology> <speaker> 0.0 0.0 0.0 _ <mention> <features> <corefence-label>

class Word:
    def __init__(self, tokens):
        self.filename = tokens[0]
        self.global_id = tokens[1]
        self.local_id = int(tokens[2]) - 1
        self.text = tokens[3]
        self.disfluency = tokens[4]
        self.postag = tokens[5]
        self.named_entity = tokens[6]
        self.dep_label = tokens[8]
        self.parent = int(tokens[9]) - 1
        self.text_id = tokens[10]
        self.lemma = tokens[11]
        self.morphology = tokens[12]
        self.speaker = tokens[13]
        self.mention = tokens[18] if len(tokens) > 18 else None
        self.coref_features = tokens[19] if len(tokens) > 19 else None
        self.coref_label = tokens[20] if len(tokens) > 20 else None

    def has_parent(self, sentence, criterion=lambda x: True, blocker=lambda x: False):
        node = self.parent
        visited = set()
        while node >= 0 and node < len(sentence) and node not in visited and not criterion(sentence[node]) and not blocker(sentence[node]):
            visited.add(node)
            node = sentence[node].parent
        if node < 0 or node >= len(sentence):
            return False
        if blocker(sentence[node]):
            return False
        return criterion(sentence[node])

    def get_phrase(self, sentence, blocker=lambda x: False):
        output = []
        for word in sentence:
            if word.has_parent(sentence, lambda x: x == self, blocker):
                if not blocker(word):
                    output.append(word)
        output = sorted(output + [self], key=lambda x: x.local_id)
        return output

# load a Tsv file and return an array of sentences containing each an array of words
def load(filename):
    sentences = []
    sentence = []
    with open(filename) as fp:
        for line in fp:
            line = line.strip()
            if line == '':
                if len(sentence) > 0:
                    sentences.append(sentence)
                sentence = []
            else:
                word = Word(line.split('\t'))
                sentence.append(word)
    return sentences

# load a Tsv containing each an array of words
def charge(conv):
    sentences = []
    sentence = []
    for line in conv:
        line = line.strip()
        if line == '':
            if len(sentence) > 0:
                sentences.append(sentence)
            sentence = []
        else:
            word = Word(line.split('\t'))
            sentence.append(word)
    return sentences

def importNames():
    fic = open("source/icsiboost.names", "r")
    preNames = []
    for line in fic:
        preNames.append(line.strip())
    
    names = preNames[0].replace(".", "").split(", ")
    return names

def importScores(sc):
    score=[]
    scores=[]
    for line in sc:
        t=[]
        f=[]
        t.append(line.split()[0])
        f=line.split()[19:]
        score=t+f
        scores.append(score)
    
    return scores 

def importData(dt):
    data=[]
    for line in dt:
        data.append(line)
    return data 

def linkSlots(scores, data, names, convID, s):
    seuil = float(s)
    slots={}
    convID=convID.replace(".tsv", "")

    for i in range(len(scores)):
        slot=scores[i]
        if int(slot[0]) != 1 and data[i].split(",")[0].replace(".tsv", "") == convID.strip():
            if names[int(slot[0])-int(1)].strip() not in slots:
                if float(slot[int(slot[0])]) >= seuil:
                    slots[names[int(slot[0])-int(1)].strip()] = [data[i].split(",")[8], slot[int(slot[0])].strip()]
                else:
                    continue
            else:
                if float(slot[int(slot[0])]) >= seuil:
                    if slots[names[int(slot[0])-int(1)].strip()][1] > slot[int(slot[0])-int(1)].strip():
                        continue
                    else:
                        slots[names[int(slot[0])-int(1)].strip()][1] = [data[i].split(",")[8], slot[int(slot[0])].strip()]

        elif int(slot[0]) == 1 and data[i].split(",")[0].replace(".tsv", "") == convID:
            for j in range(len(slot[2:])):
                if float(slot[2+j].strip()) >= seuil:
                    if names[2+j-1].strip() not in slots:
                        slots[names[2+j-1].strip()] = [data[i].split(",")[8], slot[2+j].strip()]
#                        print names[2+j-1].strip(), data[i].split(",")[1], slot[2+j].strip()
                    else:
                        if slots[names[2+j-1].strip()][1] > slot[2+j]:
                            continue
                        else:
                            slots[names[2+j-1].strip()] = [data[i].split(",")[8], slot[2+j].strip()]
 
    finals = {}
#    print slots
    for itm in slots:
        finals["$"+itm.upper()] = slots[itm][0]
    
    return finals

def importTemplates(convID):
    fic = open("source/syn.annot", "r")
    raw = []
    for line in fic:
        if line.split(" ")[1] == convID:
            continue  
        else:
            raw.append(line)

    templates = []
    stopPrinting = 0
    for line in raw:
        tmp = ""
        itm = line.split(" ")
        for word in itm:
            if "<a" in word:
                stopPrinting = 1
            if stopPrinting == 0:
#                print word, 
                tmp += word.strip()+" "
            if "a>" in word:
                stopPrinting = 0
            if 'title="$' in word and stopPrinting == 1:
#                print word.split('"')[1],
                tmp += word.split('"')[1].strip()+" "
        templates.append(tmp)

    template = []
    for itm in templates:
        tmpplt = " ".join(itm.split(" ")[3:])
        for tpl in tmpplt.split(". "):
            if tpl != "":
                template.append(tpl.strip())

    return template

def fillTemplate(templates, ficsi, flib, fnames, slot, s):
    import operator
    slots = slot
#    print >> sys.stderr, "##### SLOTS #####", slots

#    prefixe = "FR_"+convID
    
#    mmr = myMMR.myMMR()

#    if slots == {}:
#        text="fullText_cleaned/"+convID+".fullText"
#        mmrresult=mmr.MMR(text ,0.7).replace("+", " ").strip()
#        print prefixe, '\t'+method+'\t"'+mmrresult+'"'
#        #print prefixe, "\t"+method+"\tAppel sans détection."
#        return
    
    scoreTemplateSlot = {}
    for template in templates:
        cpt = 0
        for itm in slots:
            if itm in template:
                cpt += 1
        maxslot=0
        if cpt >= len(slots)-2:
            if template.count("$") == 0: continue
            maxslot = float(float(cpt)/float(template.count("$")))
            if "Demande" in " ".join(template.split(" ")[0:3]):
                maxslot += 1
            
            scoreTemplateSlot[template] = maxslot
        else:  ### A VOIR
            if template.count("$") == 0: continue
            maxslot = float(float(cpt)/float(template.count("$")))
            scoreTemplateSlot[template] = maxslot


    template = max(scoreTemplateSlot.iteritems(), key=operator.itemgetter(1))[0]
#    print >> sys.stderr, "##### TEMPLATE #####", template

    syn = ""

#    print prefixe, "\t"+method+"\t",
#    syn=prefixe.replace(".tsv", "").replace("FR_", "")+"\t"+method+"\t"
    for word in template.split(" "):  #### A VOIR ICI 4/3
        if "$" in word:
            try:
#                print slots[word].strip(), ### commenter
                syn=syn+slots[word].strip()+" "
                del slots[word]
            except(KeyError):
#                print word.split("$")[1].lower(),
                syn=syn+word.split("$")[1].lower()+" "
        else:
#            print word,
            syn=syn+word+" "

    ###
    ### Toujour pas jolie
    ###

#    print >> sys.stderr, "##### SLOTS #####", slots   
    if slots == {}:
        return syn
    
    scoreTemplateSlot = {}
    for template in templates:
        cpt = 0
        for itm in slots:
            if itm in template:
                cpt += 1
        maxslot=0
        if cpt >= 1:
            if template.count("$") == 0: continue
            maxslot = float(float(cpt)/float(template.count("$")))
            scoreTemplateSlot[template] = maxslot
            if maxslot == 0:
                return

    
    if scoreTemplateSlot == {}: return
    template = max(scoreTemplateSlot.iteritems(), key=operator.itemgetter(1))[0]
#    print >> sys.stderr, "##### TEMPLATE #####", template   
 
#    print ".", 
    syn=syn+". " 
    for word in template.split(" "):
        if "$" in word:
            try:
#                print slots[word].strip(), ### commenter
                syn=syn+slots[word].strip()+" "
                del slots[word]
            except(KeyError):
#                print word.split("$")[1].lower(),
                syn=syn+word.split("$")[1].lower()+" "
        else:
#            print word,
            syn=syn+word+" "
    syn=syn+" ."
    return syn


def summarize(conv, seuil, convID):
    import predsyn as ps
    import os
    ### extractor
    #sentences = ps.tsv.load(convID)
    #sentences = charge(conv) ## > charge la conversation
    ## sentences =  instance de tsv.Word()
    preSentences = ps.output_phrases(sentences, convID) ## >>> créé le source/icsiboost.test

    ### Predictor
    res=os.popen("icsiboost -S source/icsiboost -W 3 -C --posteriors < source/icsiboost.test") ## > Prédiction
    res=res.read().strip().split("\n") ## > prédiction dans le bon format

    ### productor
    names=importNames() ## Importe les noms du fichier source/icsiboost.names
    scores=importScores(res) ## Les scores de prédiction dans le format qui va bien 
    data=importData(preSentences) ## Importe les données avec les features
    slots=linkSlots(scores, data, names, convID, seuil) ## lie les scores avec les slots en fonction du seuil
    #print "slots=", slots ## $> slots= {'$TRANSPORT': 'un bus'}
    templates=importTemplates(convID) ## Sélectionne et rempli un template
    synopsis=fillTemplate(templates, scores, data, names, slots, seuil) ## Génère le synopsis   
    
    return synopsis 

